//Author: Prof. Alex Kutske
//Funçoes adicionais eh_espelho e cria_espelho: Lucas Frison Goncalves

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct arvore {
   char info;
   struct arvore *esq;
   struct arvore *dir;
} Arvore;


Arvore*  cria_arv_vazia (void);
Arvore*  arv_constroi (char c, Arvore* e, Arvore* d);
int      verifica_arv_vazia (Arvore* a);
Arvore*  arv_libera (Arvore* a);
int      arv_pertence (Arvore* a, char c);
void     arv_imprime (Arvore* a);

Arvore* cria_arv_vazia (void) {
   return NULL;
}

Arvore* arv_constroi (char c, Arvore* e, Arvore* d) {
  Arvore* a = (Arvore*)malloc(sizeof(Arvore));
  a->info = c;
  a->esq = e;
  a->dir = d;
  return a;
}

int verifica_arv_vazia (Arvore* a) {
  return (a == NULL);
}

Arvore* arv_libera (Arvore* a) {
  if (!verifica_arv_vazia(a)) {
    arv_libera (a->esq);
    arv_libera (a->dir);
    free(a);
  }
  return NULL;
}

Arvore * cria_espelho(Arvore * arvore) {
    if (!verifica_arv_vazia(arvore)) {
        cria_espelho(arvore->esq);
        cria_espelho(arvore->dir);
        Arvore * temp;
        temp = arvore->dir;
        arvore->dir = arvore->esq;
        arvore->esq = temp;
    }
}

int eh_espelho(Arvore* arv1, Arvore* arv2) {
    if (arv1 == NULL && arv2 == NULL) 
        return 1;
    if (arv1->info == arv2->info)
        if (eh_espelho(arv1->esq, arv2->dir) && eh_espelho(arv1->dir, arv2->esq))
            return 1;
    return 0;        
}

int main (int argc, char *argv[]) {
  char* espelho[2] = {"Falso", "Verdadeiro"};
  Arvore *a, *a1, *a2, *a3, *a4, *a5;
  a1 = arv_constroi('d',cria_arv_vazia(),cria_arv_vazia());
  a2 = arv_constroi('b',cria_arv_vazia(),a1);
  a3 = arv_constroi('e',cria_arv_vazia(),cria_arv_vazia());
  a4 = arv_constroi('f',cria_arv_vazia(),cria_arv_vazia());
  a5 = arv_constroi('c',a3,a4);
  a  = arv_constroi('a',a2,a5);
  printf("A2 eh espelho de a1? %s\n", espelho[eh_espelho(a1, a2)]);
  a2 = cria_espelho(a1);
  printf("E agora? %s", espelho[eh_espelho(a1, a2)]);
  return 0;
}
